import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String pass = req.getParameter("password");

        if (email.equals("slava@gmail.com") && pass.equals("myPass")) {
            req.getSession().setAttribute("registered", true);
            req.getRequestDispatcher("success.jsp").forward(req, resp);
        } else {
            req.getSession().setAttribute("registered", false);
            req.getRequestDispatcher("fail.jsp").forward(req, resp);
        }
    }
}
