<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title></title>
  </head>
  <body>
  <c:choose>
    <c:when test="${status}">
      <h1>You're registered!</h1>
    </c:when>
    <c:otherwise>
      <h1>You're not registered!</h1><br/>
      <h2>Clik <a href="/my">here</a> to register!</h2>
    </c:otherwise>
  </c:choose>
  </body>
</html>
